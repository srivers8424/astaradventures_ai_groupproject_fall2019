﻿/*
* Course: CS4242 Artificial Intelligence Fall 2019
* Student name: Menelio Alvarez
* Assignment #: Group Project
* DueDate: December 2, 2019
* Signature:
* Score:
*
*/

using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AStar_Node : MonoBehaviour
{

    /// <summary>
    /// name of node
    /// </summary>
    public String name { get; set; }

    /// <summary>
    /// Indicates node is in route
    /// </summary>
    public Boolean isInRoute { get; set; }

    /// <summary>
    /// h(n) of node (Streight line distance to goal)
    /// </summary>
    public double hn { get; set; }

    /// <summary>
    /// weight of four eges North, East, South, and West  (a -1 indicates that 
    /// there is no edge in that direction.) 
    /// </summary>
    public AStar_Edge[] edges { get; set; }

    /// <summary>
    /// X index of node(Just to make some stuff easy
    /// </summary>
    public int X { get; set; }
    /// <summary>
    /// Y index of node(Just to make some stuff easy
    /// </summary>
    public int Y { get; set; }

    /// <summary>
    /// Arg constructor alows you to set name and edges
    /// </summary>
    /// <param name="name">name of node</param>
    /// <param name="edges">array of doubles for the node edges</param>
    public AStar_Node(String name, double[] edges)
    {
        this.isInRoute = false;
        this.name = name;
        GetEdgesFromDoubleArray(edges);
    }

    /// <summary>
    /// //Populates the array of edges for *this* node from a given array of doubles
    /// </summary>
    /// <param name="edgesArr"></param>
    /// <returns></returns>
    public void GetEdgesFromDoubleArray(double[] edgesArr)
    {
        this.edges = new AStar_Edge[edges.Length];
        for (int i = 0; i < edges.Length; i++)
        {
            this.edges[i] = new AStar_Edge(edgesArr[i], this);
            //Set the direction according to the index of the edge
            this.edges[i].direction = (AStar_Edge.edgeDirection)i;
        }
    }

    public void GetRandomEdges()
    {
        System.Random rand = new System.Random();
        int size = 8;
        edges = new AStar_Edge[size];
        int randIndex = UnityEngine.Random.Range(0, size);
        int randEdgeCounter = 0;

        for (int i = 0; i < size; i++)
        {
            if (i == randIndex && randEdgeCounter <= 4)
            {
                double randValue = Math.Round(((rand.NextDouble() * 4) + 1), 2);
                edges[randIndex] = new AStar_Edge(randValue, this);
            }
            else
            {
                if (edges[i] == null)
                {
                    edges[i] = new AStar_Edge(-1.0, this);
                }
            }
            //Set the direction according to the index of the edge
            //  edges[i].direction = (AStar_Edge.edgeDirection)i;
        }

        ////Randomly plot 3 edges to have weights
        //for (int i = 0; i < 3; i++)
        //{
        //    int randIndex = UnityEngine.Random.Range(0, edges.Length);
        //    double randValue = Math.Round(((rand.NextDouble() * 4) + 1), 2);
        //    edges[randIndex] = new AStar_Edge(randValue, this);
        //}
    }

    /// <summary>
    /// Arg constructor allows you to sest the name and no edges
    /// </summary>
    /// <param name="name">name of node</param>
    public AStar_Node(String name)
    {
        this.isInRoute = false;
        this.name = name;
        double[] edges = { -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0 };
        //this.edges = edges;
    }

    public AStar_Node(int xIndex, int yIndex)
    {
        this.isInRoute = false;
        this.X = xIndex;
        this.Y = yIndex;
        this.name = "x: " + xIndex.ToString() + " y: " + yIndex.ToString();
        double[] edges = { -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0, -1.0 };
        //this.edges = edges;
        GetRandomEdges();
    }

}

