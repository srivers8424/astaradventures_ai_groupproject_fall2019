﻿/*
* Course: CS4242 Artificial Intelligence Fall 2019
* Student name: Menelio Alvarez
* Assignment #: Group Project
* DueDate: December 2, 2019
* Signature:
* Score:
*
*/

using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AStar_Route
{
    //nodes in this route
    public AStar_Node[] nodes { get; set; }

    //Name of this route
    public String name { get; set; }

    //f(n) of last node in this route
    public double fn { get; set; }
    /*total g(n) from all edge in route 
     * (must be set from outside as no edges stored in route
     * and is necessary for calculating f(n)*/
    public double totalGn { get; set; }



    /// <summary>
    /// Arg Constructor for single node route g(n) will be set 
    /// to zero
    /// </summary>
    /// <param n="n"> First node in route</param>
    public AStar_Route(AStar_Node n)
    {
        nodes = new AStar_Node[0];
        nodes = AStar_Utils.AppentToArray(this.nodes, n);
        this.totalGn = 0;
        calculateFn();
    }

    /// <summary>
    /// Arg Constructor for route with more than one node
    /// </summary>
    /// <param name="nodes">nodes in the route</param>
    /// <param name="totalGn">total g(n) for this route</param>

    public AStar_Route(AStar_Node[] nodes, double totalGn)
    {
        this.nodes = nodes;
        this.totalGn = this.totalGn + totalGn;
        calculateFn();
    }

    /// <summary>
    /// Adds a node to the route 
    /// </summary>
    /// <param name="n">n is the node to add</param>
    /// <param name="gn">gn is the gn of edge connect to added node</param>
    public void addNode(AStar_Node n, double gn)
    {
        nodes = AStar_Utils.AppentToArray(this.nodes, n);
        this.totalGn = this.totalGn + gn;
        this.calculateFn();
    }

    //caluculates fn of last node in the route
    private void calculateFn()
    {
        fn = totalGn + nodes[nodes.Length - 1].hn;
    }


}

