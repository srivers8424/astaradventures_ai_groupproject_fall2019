﻿/*
* Course: CS4242 Artificial Intelligence Fall 2019
* Student name: Menelio Alvarez
* Assignment #: Group Project
* DueDate: December 2, 2019
* Signature:
* Score:
*
*/

using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AStar_Edge : MonoBehaviour
{
    //Sets the direction the edge is facing from the node
    public enum edgeDirection { NORTH, SOUTH, EAST, WEST, NORTHWEST, NORTHEAST, SOUTHWEST, SOUTHEAST }
    //Used to determine which way the edge is facing relative to the node
    public edgeDirection direction { get; set; }
    public bool connected { get; set; }
    //global var
    public double gn { get; set; }
    /*edge only point to one node, they are one directional, to have two nodes
     *  point to eachother you need two edge. One in each node*/
    public AStar_Node node { get; set; }
    /// <summary>
    /// Argument constructor
    /// </summary>
    /// <param name="gn">Cost of crossing this edge</param>
    /// <param name="node">AStar_Node this edge points to</param>
    public AStar_Edge(double gn, AStar_Node node)
    {
        this.gn = gn;
        this.node = node;
    }

}
