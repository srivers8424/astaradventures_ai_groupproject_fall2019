﻿/*
* Course: CS4242 Artificial Intelligence Fall 2019
* Student name: Menelio Alvarez
* Assignment #: Group Project
* DueDate: December 2, 2019
* Signature:
* Score:
*
*/
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;

public class AStar : MonoBehaviour
{
    /// <summary>
    /// Set the boolian value of isInRoute in node in graph if they appear
    /// in shortest route to given goal node
    /// </summary>
    /// <param name="graph"> Graph of nodes</param>
    /// <param name="startX">X coord/index of starting node</param>
    /// <param name="startY">Y coord/index of starting node</param>
    /// <param name="goalX">X coord/index of goal node</param>
    /// <param name="goalY">Y coord/index of goal node</param>
    public static void Search(AStar_Graph
        graph, int startX, int startY, int goalX, int goalY)
    {

        //open
        AStar_Route[] open = new AStar_Route[0];
        //current route, initialized with first node
        AStar_Route currRoute = new AStar_Route(graph.nodes[startX, startY]);
        //current X, and Y
        int currX = startX;
        int currY = startY;

        while ((currX != goalX) || (currY != goalY))
        {
            //create new routes from current route
            for (int i = 0; i < currRoute.nodes[currRoute.nodes.Length - 1].edges.Length; i++)
            {
                if (currRoute.nodes[currRoute.nodes.Length - 1].edges[i].gn != -1)
                {//if there is and edge(dose not = -1)
                    if (i == 0)//if North node
                    {

                        open = AStar_Utils.AppentToArray(open,
                            new AStar_Route(AStar_Utils.AppentToArray(currRoute.nodes,
                            graph.nodes[(currY - 1), currX]),
                            currRoute.totalGn + graph.nodes[currY, currX].edges[i].gn
                            ));
                    }
                    else if (i == 1)//if East node
                    {
                        open = AStar_Utils.AppentToArray(open,
                            new AStar_Route(AStar_Utils.AppentToArray(currRoute.nodes,
                            graph.nodes[currY, (currX + 1)]),
                            currRoute.totalGn + graph.nodes[currY, currX].edges[i].gn
                            ));
                    }
                    else if (i == 2)//if South node
                    {
                        open = AStar_Utils.AppentToArray(open,
                            new AStar_Route(AStar_Utils.AppentToArray(currRoute.nodes,
                            graph.nodes[(currY + 1), currX]),
                            currRoute.totalGn + graph.nodes[currY, currX].edges[i].gn
                            ));
                    }
                    else if (i == 3)//if West node
                    {

                        open = AStar_Utils.AppentToArray(open,
                             new AStar_Route(AStar_Utils.AppentToArray(currRoute.nodes,
                             graph.nodes[currY, (currX - 1)]),
                             currRoute.totalGn + graph.nodes[currY, currX].edges[i].gn
                             ));
                    }
                }
            }
            //find cheapest route and make the next current route
            currRoute = open[0];
            int indexOfCheapest = 0;
            for (int i = 0; i < open.Length; i++)
            {
                if (currRoute.fn > open[i].fn)
                {
                    currRoute = open[i];
                    indexOfCheapest = i;

                }
            }
            //remove cheapest from open
            open = AStar_Utils.RemoveAt(open, indexOfCheapest);
            currX = currRoute.nodes[currRoute.nodes.Length - 1].X;
            currY = currRoute.nodes[currRoute.nodes.Length - 1].Y;


        }
        //set values of isInRoute
        for (int i = 0; i < currRoute.nodes.Length; i++)
        {
            graph.nodes[currRoute.nodes[i].Y, currRoute.nodes[i].X].isInRoute = true;
        }

    }
}


