﻿/*
* Course: CS4242 Artificial Intelligence Fall 2019
* Student name: Menelio Alvarez
* Assignment #: Group Project
* DueDate: December 2, 2019
* Signature:
* Score:
*
*/

using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AStar_Graph
{
    /// <summary>
    /// 2D array of nodes
    /// </summary>
    public AStar_Node[,] nodes { get; set; }

    /// <summary>
    /// Args Constructor 
    /// </summary>
    /// <param name="maxX">X deminsion of 2d node array</param>
    /// <param name="maxY">Y deminsion of 2d node array</param>
    public AStar_Graph(int maxX, int maxY)
    {
        this.nodes = new AStar_Node[maxX, maxY];
        generateGraph();
    }

    /// <summary>
    /// Args Constructor
    /// </summary>
    /// <param name="nodes">2D Array of nodes in graph</param>
    public AStar_Graph(AStar_Node[,] nodes)
    {
        this.nodes = nodes;
    }

    /// <summary>
    /// Sets the hn of all the nodes in the graph/2d array of nodes
    /// </summary>
    /// <param name="x">x coor or index of goal node</param>
    /// <param name="y">y coor or index of goal node</param>
    public void setHnOfNodes_And_XY(int x, int y)
    {
        for (int i = 0; i < nodes.GetLength(0); i++)
        {
            for (int j = 0; j < nodes.GetLength(1); j++)
            {
                if(nodes[i,j] == null)
                {
                    UnityEngine.Debug.Log("NULL GRAPH FOUND IN ASTAR h(n) IMPLEMENTATION at INDEX: " + i + " , " + j);
                }
                nodes[i, j].hn = Math.Round((Math.Sqrt(Math.Pow((x - i), 2) + Math.Pow((y - j), 2))), 2);
                nodes[i, j].X = j;
                nodes[i, j].Y = i;
            }
        }
    }


    private void generateGraph()
    {
        System.Random rand = new System.Random();
        for (int i = 0; i < nodes.GetLength(0); i++)
        {
            for (int j = 0; j < nodes.GetLength(1); j++)
            {
                this.nodes[i, j] = new AStar_Node("n" + i + "," + j);
                //North
                if (i == 0)
                {
                    nodes[i, j].edges[0].gn = -1;
                }
                else
                {
                    double val = Math.Round(((rand.NextDouble() * 4) + 1), 2);
                    nodes[i, j].edges[0].gn = val;
                }
                //West 
                if (j == 0)
                { 
                    nodes[i, j].edges[1].gn = -1;
                }
                else
                {
                    double val = Math.Round(((rand.NextDouble() * 4) + 1), 2);
                    nodes[i, j].edges[1].gn = val;
                }
                //South
                if (i == (nodes.GetLength(0) - 1))
                {
                    nodes[i, j].edges[2].gn = -1;
                }
                else
                {
                    double val = Math.Round(((rand.NextDouble() * 4) + 1), 2);
                    nodes[i, j].edges[2].gn = val;
                }
                //East
                if (j == (nodes.GetLength(1) - 1))
                {
                    nodes[i, j].edges[3].gn = -1;
                }
                else
                {
                    double val = Math.Round(((rand.NextDouble() * 4) + 1), 2);
                    nodes[i, j].edges[3].gn = val;
                }

            }
        }
    }
}


