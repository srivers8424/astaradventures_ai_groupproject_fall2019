﻿/*
* Course: CS4242 Artificial Intelligence Fall 2019
* Student name: Sonya Rivers-Medina
* Assignment #: Group Project
* DueDate: December 2, 2019
* Signature:
* Score:
*
*/
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GridNode : MonoBehaviour, IComparable<GridNode>
{
    public enum status { DEFAULTNODE, CURRENTNODE, TARGETNODE }
    public AStar_Node node { get; set; }
    public List<NodeEdge_UIUX> nodeEdges;
    public AStar_Edge[] edges;
    public Image img;
    public List<GridNode> neighbors { get; set; }
    public GridNode closestNeighbor { get; set; }
    [SerializeField] private Sprite defaultNodeImg;
    [SerializeField] private Sprite currentNodeImg;
    [SerializeField] private Sprite targetNodeImg;
    public bool visited = false;
    public double hn { get; set; }
    private void Awake()
    {
        node = GetComponent<AStar_Node>();
    }

    public void InitializeGridNode(int gridX, int gridY)
    {
        node = new AStar_Node(gridX, gridY);
        SetImage(status.DEFAULTNODE);
        SetNodeEdges();
    }

    public void SetVisited()
    {
        visited = true;
    }

    /// <summary>
    /// Sets the H of N using Euclidean Distance Heuristic (Pythagorean theorem)
    /// </summary>
    public double SetNodeHN(List<GridNode> gridList, GridNode destinationNode)
    {
        int size = gridList.Count;
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                int index = j + i * size;
                hn = Math.Round((Math.Sqrt(Math.Pow((node.X - i), 2) + Math.Pow((destinationNode.node.Y - j), 2))), 2);
            }
        }

        node.hn = hn;
        return hn;
    }

    public void SetNodeStatus(status tag)
    {
        SetImage(tag);

        switch (tag)
        {
            case status.DEFAULTNODE:

                break;
            case status.CURRENTNODE:

                break;
            case status.TARGETNODE:

                break;
        }
    }

    /// <summary>
    /// Sets the node's image according to its current status in the grid
    /// </summary>
    /// <param name="tag"></param>
    private void SetImage(status tag)
    {
        switch (tag)
        {
            case status.DEFAULTNODE:
                img.sprite = defaultNodeImg;
                break;
            case status.CURRENTNODE:
                img.sprite = currentNodeImg;
                break;
            case status.TARGETNODE:
                img.sprite = targetNodeImg;
                break;
        }
    }


    private void SetNodeEdges()
    {
        for (int i = 0; i < node.edges.Length; i++)
        {
            for (int j = 0; j < nodeEdges.Count; j++)
            {
                if (node.edges[i].gn > -1.0)
                {
                    nodeEdges[j].SetEdge(node, node.edges[i].gn, i);
                }
            }
        }

        GetNeighbors();
    }


    public AStar_Edge[] GetEdges()
    {
        edges = new AStar_Edge[nodeEdges.Count];
        for (int i = 0; i < nodeEdges.Count; i++)
        {
            edges[i] = nodeEdges[i].edge;
        }

        node.edges = edges;
        return edges;
    }

    public double[] GetNeighborDistances()
    {
        List<GridNode> neighbors = GetNeighbors();
        double[] distances = new double[neighbors.Count];
        for (int i = 0; i < neighbors.Count; i++)
        {
            distances[i] = GetNeighborEdgeDistance(neighbors[i]);
        }

        return distances;
    }

    public List<GridNode> GetNeighbors()
    {
        neighbors = new List<GridNode>();
        foreach (NodeEdge_UIUX ne in nodeEdges)
        {
            if (ne.connected && ne.connectedGridNode != null)
            {
                neighbors.Add(ne.connectedGridNode);
            }
        }
        return neighbors;
    }

    /// <summary>
    /// Returns closest neighbor from an array of double neighbor distances
    /// </summary>
    /// <param name="neighborDistances"></param>
    /// <returns></returns>
    public GridNode GetClosestNeighbor()
    {
        neighbors = GetNeighbors();
        GridNode closestNeighbor = neighbors[0];
        double[] dist = GetNeighborDistances();

        if(dist.Length != neighbors.Count)
        {
            Debug.Log("INACCURATE NEIGHBOR COUNT IN ATTEMPT TO GET CLOSEST NEIGHBOR! TRY AGAIN!");
            return null;
        }

        for (int i = 1; i <= neighbors.Count; i++)
        {
            if (dist[i] <= GetNeighborEdgeDistance(closestNeighbor))
            {
                closestNeighbor = neighbors[i];
            }
        }
        return closestNeighbor;
    }

    public double GetNeighborEdgeDistance(GridNode neighbor)
    {
        double dist = -1;
        //if the grid node isn't in the neighbors list, they're not a REAL neighbor!
        if (!neighbors.Contains(neighbor))
        {
            Debug.Log("NOT A REAL NEIGHBOR!!! TERMINATING DISTANCE CHECK!");
            return dist;
        }

        for (int i = 0; i < neighbor.nodeEdges.Count; i++)
        {
            if(neighbor.nodeEdges[i].connectedGridNode == this)
            {
                dist = neighbor.nodeEdges[i].connectedWeight;
                return dist;
            }
        }

        //Otherwise return -1
        return dist;
    }

    public GridNode GetClosestNeighborFromGridNodeList(List<GridNode> neighbors)
    {
        GridNode closestNeighbor = neighbors[0];

        for (int i = 1; i <= neighbors.Count; i++)
        {
            if (neighbors[i].CompareTo(closestNeighbor) < 0)
            {
                closestNeighbor = neighbors[i];
            }
        }
        return closestNeighbor;
    }


    public NodeEdge_UIUX GetSmallestConnectedEdge()
    {
        NodeEdge_UIUX smallest = nodeEdges[0];
        for (int i = 1; i <= nodeEdges.Count; i++)
        {
            if (nodeEdges[i].connected && nodeEdges[i].CompareTo(smallest.edge) < 0)
            {
                smallest = nodeEdges[i];
            }
        }
        return smallest;
    }



    public int CompareTo(GridNode other)
    {
        int comparison = GetSmallestConnectedEdge().CompareTo(other.GetSmallestConnectedEdge().edge);

        if (comparison > 0)
        {
            return -1;
        }
        else if (comparison == 0)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }
}
