﻿/*
* Course: CS4242 Artificial Intelligence Fall 2019
* Student name: Sonya Rivers-Medina
* Assignment #: Group Project
* DueDate: December 2, 2019
* Signature:
* Score:
*
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Player : MonoBehaviour
{
    public static Player instance;
    public AudioClip playerMoveClip;
    public AudioSource playerAudio;
    private Animator anim;
    private Transform movePosition;
    private Vector3 agentVelocity = Vector3.zero;
    float timeOfTravel = 3.5f; //time after object reach a target place 
    float currentTime = 0; // actual floting time 
    float normalizedValue = 0.0f;
    private RectTransform rectTransform; //getting reference to this component 
    
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

        anim = GetComponent<Animator>();
    }

    private void Start()
    {
        rectTransform = GetComponent<RectTransform>();
     
    }

    public void MoveToTarget(GridNode targetNode)
    {
        Move(targetNode.gameObject.transform);
    }

    private void Move(Transform newPosition)
    {
        movePosition = newPosition;
        StartCoroutine(LerpObject());

    }

    private IEnumerator LerpObject()
    {
        Debug.Log("moving to ( " + movePosition.position.x + ", " + movePosition.position.y + " , " + movePosition.position.z + " ) " );
        Vector3[] path = new Vector3[1];
        path[0] = movePosition.position;
        transform.DOLocalPath(path, timeOfTravel, PathType.Linear);
        //Play move sound
        if (playerMoveClip != null && playerAudio != null)
        {
            playerAudio.clip = playerMoveClip;
            playerAudio.Play();
        }
        //Play move animation
        anim.SetBool("moving", true);

        transform.DOPath(path, timeOfTravel, PathType.Linear);
        yield return null;

        //Vector3 startPosition = transform.localPosition;
        //while (currentTime <= timeOfTravel)
        //{
        //    currentTime += Time.deltaTime;
        //    normalizedValue = currentTime / timeOfTravel; // we normalize our time 

        //    transform.localPosition = Vector3.SmoothDamp(transform.localPosition, movePosition.localPosition, ref agentVelocity, 0.05f);
        //    yield return null;
        //}

        if (transform.localPosition == movePosition.localPosition)
        {
            //Stop move sound
            if (playerAudio != null && playerAudio.isPlaying)
            {
                playerAudio.Stop();
            }

            //Stop move animation
            anim.SetBool("moving", false);
        }
    }
}
