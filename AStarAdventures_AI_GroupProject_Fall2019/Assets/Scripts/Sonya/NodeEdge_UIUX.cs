﻿/*
* Course: CS4242 Artificial Intelligence Fall 2019
* Student name: Sonya Rivers-Medina
* Assignment #: Group Project
* DueDate: December 2, 2019
* Signature:
* Score:
*
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class NodeEdge_UIUX : MonoBehaviour, IComparable<AStar_Edge>
{
    #region Public fields
    public AStar_Edge edge { get; set; }
    public TextMeshProUGUI weightTxt;
    public enum edgeDirection { NORTH, SOUTH, EAST, WEST, NORTHWEST, NORTHEAST, SOUTHWEST, SOUTHEAST }
    public GridNode gridNode;
    public GridNode connectedGridNode;
    public edgeDirection direction;
    public Vector3 connectionPath_startPosition;
    public Vector3 connectionPath_EndPosition;
    public bool connected = false;
    public bool edgeDrawn = false;
    public double Weight { get; set; }
    public double connectedWeight { get; set; }
    #endregion

    #region Private fields
    private Image edgeImg;
    private LineRenderer line;
    #endregion


    private void Awake()
    {
        edgeImg = GetComponent<Image>();
        line = GetComponent<LineRenderer>();
        line.startWidth = 2.5f;
        line.endWidth = 2.5f;
        connectedWeight = 0;
    }

    private void OnTriggerEnter(Collider other)
    {
        //If a node is already connected, no need to redraw the lines!
        if(connected)
        {
            return;
        }

        switch (other.tag)
        {
            case "NodeEdge_North":
                if (direction == edgeDirection.SOUTH)
                {
                    connected = true;

                }
                break;
            case "NodeEdge_NorthWest":
                if (direction == edgeDirection.SOUTHEAST)
                {
                    connected = true;
                }
                break;
            case "NodeEdge_NorthEast":
                if (direction == edgeDirection.SOUTHWEST)
                {
                    connected = true;
                }
                break;
            case "NodeEdge_South":
                if (direction == edgeDirection.NORTH)
                {
                    connected = true;
                }
                break;
            case "NodeEdge_SouthWest":
                if (direction == edgeDirection.NORTHEAST)
                {
                    connected = true;
                }
                break;
            case "NodeEdge_SouthEast":
                if (direction == edgeDirection.NORTHWEST)
                {
                    connected = true;
                }
                break;
            case "NodeEdge_West":
                if (direction == edgeDirection.EAST)
                {
                    connected = true;
                }
                break;
            case "NodeEdge_East":
                if (direction == edgeDirection.WEST)
                {
                    connected = true;
                }
                break;
        }


        if (connected)
        {
            ////  Debug.Log("connected nodes at index: " + gridNode.node.name + " Node Edge " + this.name
            //      /*+ " and " + other.GetComponent<NodeEdge_UIUX>().name*/);

            connectionPath_startPosition = transform.position;
            connectionPath_EndPosition = other.transform.position;
            DrawEdge();
           
            NodeEdge_UIUX otherEdge = other.GetComponent<NodeEdge_UIUX>();
            otherEdge.connected = true;
            connectedGridNode = otherEdge.gridNode;
            connectedWeight += otherEdge.Weight;
            otherEdge.connectedWeight = connectedWeight;
        }

    }

    public void SetEdge(AStar_Node node, double weight, int index)
    {
        edge = new AStar_Edge(weight, node);
        //SetDirection(index);
        SetWeight(weight);
    }

    public AStar_Edge GetEdge()
    {
        return edge;
    }
    private void DrawEdge()
    {
        if (connected && !edgeDrawn)
        {
            if (line.positionCount >= 1)
            {
                line.SetPosition(0, connectionPath_startPosition);
                line.SetPosition(1, connectionPath_EndPosition);
            }

            edgeImg.DOFillAmount(1.0f, 2.5f);
            weightTxt.DOFade(1.0f, 3.5f);
        }

        edgeDrawn = true;
    }


    private void EraseEdge()
    {
        edgeImg.DOFillAmount(0.0f, 2.5f);
        weightTxt.DOFade(0.0f, 3.5f);
    }

    /// <summary>
    /// Sets the node edge's direction based on its index. Called by GridNode
    /// in its awake function
    /// </summary>
    /// <param name="index"></param>
    private void SetDirection(int index)
    {
        direction = (edgeDirection)index;
    }

    /// <summary>
    /// Assigns the weight value to the TMP component attached to this edge
    /// </summary>
    /// <param name="weight"></param>
    private void SetWeight(double weight)
    {
        weight = weight;
        weightTxt.text = weight.ToString();
    }


    /// <summary>
    ///  Compares two AStar_Edge objects.
    ///    If less than 0, then this edge < other
    ///    If greater than 0, then this edge > other
    ///    If 0, then this edge == other
    /// <param name="edge.gn"></param>
    /// <param name="other"></param>
    /// <returns></returns>
    public int CompareTo(AStar_Edge other)
    {
        if (edge.gn < other.gn)
        {
            return -1;
        }
        else if (edge.gn == other.gn)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }
}
