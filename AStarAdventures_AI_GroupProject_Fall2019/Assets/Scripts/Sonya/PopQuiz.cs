﻿
/*
* Course: CS4242 Artificial Intelligence Fall 2019
* Student name: Sonya Rivers-Medina
* Assignment #: Group Project
* DueDate: December 2, 2019
* Signature:
* Score:
*
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PopQuiz : MonoBehaviour
{
    public TextMeshProUGUI chosenAlgorithmText;

    public string correctAnswer = "";
    public string playerAnswer = "";
    public bool correct = false;
    private string aStar = "A* Algorithm";
    private string dijkstra = "Dijkstra's Algorithm";
    public void SetPlayerAnswer(string answer)
    {
        playerAnswer = answer;
        chosenAlgorithmText.text = playerAnswer;
    }

    public void SetCorrectAnswer(string answer)
    {
        correctAnswer = answer;
        if(correctAnswer == playerAnswer)
        {
            correct = true;
        }
    }

    public void CheckIfCorrect()
    {
        if(correct)
        {
            ActivateWinnerMessage();
        }
        else
        {
            ActivateLoseMessage();
        }
    }

    private void ActivateWinnerMessage()
    {
        Debug.Log("CONGRATULATIONS! YOU WIN");
    }

    private void ActivateLoseMessage()
    {
        Debug.Log("SORRY! YOU LOSE!");
    }


}
