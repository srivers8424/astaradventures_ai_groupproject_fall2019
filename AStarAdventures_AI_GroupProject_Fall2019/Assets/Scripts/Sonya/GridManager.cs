﻿/*
* Course: CS4242 Artificial Intelligence Fall 2019
* Student name: Sonya Rivers-Medina
* Assignment #: Group Project
* DueDate: December 2, 2019
* Signature:
* Score:
*
*/
using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
using Doozy;

public class GridManager : MonoBehaviour
{
    public bool useDijkstra = false;
    public static GridManager Grid; //instance to be referred to by the AI Vacuum
    public GameObject gridSpace; //Prefab reference to grid space
    public GameObject[] spaces;
    public GameObject NodePrefab;
    public GameObject PlayerPrefab;
    public Transform PlayerSpawnPos;
    public List<GameObject> gridObjects;
    public List<GridNode> graphNodes;
    public AStar_Node[] pathNodes;
    public float moveX_Distance = 100;
    public float moveY_Distance = 100;
    public GameObject startBtn;
    public TextMeshProUGUI targetNodeText;
    public TextMeshProUGUI currentNodeText;
    public TextMeshProUGUI fnText_TargetNode;
    public TextMeshProUGUI fnText_CurrentNode;
    public TextMeshProUGUI gnText_TargetNode;
    public TextMeshProUGUI gnText_CurrentNode;
    public TextMeshProUGUI dijkstraTimerTxt;
    public TextMeshProUGUI aStarTimerTxt;
    public float minXPos { get; set; }
    public float maxXPos { get; set; }
    public float minYPos { get; set; }
    public float maxYPos { get; set; }
    public int gridSize = 3;
    public int maxNodes;

    private GridLayoutGroup gridLayout;

    //A* Components
    private GameObject player;
    private GridNode targetNode;
    private GridNode currentNode;
    private AStar_Graph graph;
    private AStar_Route path;
    private List<GridNode> RouteNodeList_Dijkstra;
    private List<GridNode> RouteNodeList_AStar;
    private double totalGn_targetNode;
    private double totalFn_targetNode;
    private void Awake()
    {
        if (Grid == null)
        {
            Grid = this;
        }
    }

    private void Start()
    {
        gridLayout = GetComponent<GridLayoutGroup>();
    }

    private void OnEnable()
    {
        gridLayout = GetComponent<GridLayoutGroup>();
        CreateAStarSim();
    }

    public void SetDijkstra(bool setting)
    {
        useDijkstra = setting;
    }

    public void CreateAStarSim()
    {
        SetGridSize(gridSize);
        SpawnNodes();
        StartCoroutine(DestroyStartButton());
    }

    private IEnumerator DestroyStartButton()
    {
        yield return new WaitUntil(() => transform.childCount == (gridSize * gridSize));
        Destroy(startBtn);
        SpawnPlayer();
        yield return null;
        CreateAStarRoute();
    }

    public void StepPlayer()
    {
        SetCurrentNode();
        if (currentNode != null)
        {
            Player.instance.MoveToTarget(currentNode);
        }
    }

    public void PausePlayerMove()
    {
        Player.instance.StopAllCoroutines();
    }

    public void Reset()
    {
        if (gridObjects.Count > 0)
        {
            foreach (GameObject o in gridObjects)
            {
                Destroy(o);
            }
            gridObjects.Clear();
        }

        Destroy(player);
        StartCoroutine(ResetGrid());
    }

    private IEnumerator ResetGrid()
    {
        yield return new WaitForEndOfFrame();
        gridLayout = GetComponent<GridLayoutGroup>();
        yield return null;
        gridLayout.enabled = true;
        CreateAStarSim();
    }

    //Sets a grid size for a square of the input int value
    public void SetGridSize(int size)
    {
        //Set the grid size to create a size x size grid
        gridSize = size;

        //Assign the grid layout constraint for column count
        gridLayout.constraintCount = gridSize;

        //Create the grid
        spaces = new GameObject[gridSize * gridSize];

        //Initialize graph list
        graphNodes = new List<GridNode>();


        for (int y = 0; y < gridSize; y++)
        {
            for (int x = 0; x < gridSize; x++)
            {
                int index = x + y * gridSize;
                spaces[index] = gridSpace;

                //Instantiate the prefab grid object as a child of *this* gameObject
                Instantiate(spaces[index], transform);
                gridObjects.Add(transform.GetChild(index).gameObject);

            }
        }


        //Set min/max X and Y positions
        minXPos = -100 * gridSize - 90;
        minYPos = 100 * gridSize + 90;
        maxXPos = 100 * gridSize + 90;
        maxYPos = -100 * gridSize - 90;
    }
    public Transform GetRandomGridPosition()
    {
        int randomSpace = UnityEngine.Random.Range(0, spaces.Length);
        Transform randomPosition = transform.GetChild((randomSpace)).transform;
        return randomPosition;
    }

    public GridNode GetRandomGridNode()
    {
        int randomSpace = -1;
        GridNode randomGridNode = new GridNode();

        while (randomGridNode == null)
        {
            randomSpace = GetRandomGridNodeIndex();
            if (randomSpace > 0)
            {
                randomGridNode = gridObjects[randomSpace].transform.GetChild(0).GetComponent<GridNode>();

            }
        }

        return randomGridNode;
    }

    private int GetRandomGridNodeIndex()
    {
        int randomSpace = UnityEngine.Random.Range(0, gridObjects.Count);
        if (gridObjects[randomSpace].transform.childCount == 1)
        {
            return randomSpace;
        }
        else
        {
            return -1;
        }
    }

    public void GetNodeGridIndex(GridNode gridNode, List<GameObject> gridList)
    {
        for (int y = 0; y < gridSize; y++)
        {
            for (int x = 0; x < gridSize; x++)
            {
                int index = x + y * gridSize;
                if (gridList[index].GetComponent<GridNode>() != null)
                {
                    if (gridList[index] == gridNode)
                    {
                        gridNode.node.X = x;
                        gridNode.node.Y = y;
                    }
                }

            }
        }
    }

    public void SpawnPlayer()
    {
        StartCoroutine(SetPlayerSpawnPosition());
    }

    private IEnumerator SetPlayerSpawnPosition()
    {
        //Make sure to disable the gridLayout first!
        gridLayout.enabled = false;
        yield return null;

        //Set the Grid as the spawn position parent for player to be able to move along grid
        PlayerSpawnPos.SetParent(gameObject.transform);

        //Spawn the player
        player = Instantiate(PlayerPrefab, PlayerSpawnPos);
        yield return null;

        player.transform.SetParent(gameObject.transform);
    }

    /// <summary>
    /// Assigns a target node for the player to travel towards
    /// and changes its UI/UX
    /// </summary>
    public void SetTargetNode()
    {
        targetNode = GetRandomGridNode();

        if (targetNode != null)
        {
            targetNode.SetNodeStatus(GridNode.status.TARGETNODE);
        }

        GetNodeGridIndex(targetNode, gridObjects);
        targetNodeText.text = "( " + targetNode.node.X + " , " + targetNode.node.Y + ")";
      //  fnText_TargetNode.text = System.Math.Round(totalFn_targetNode, 2).ToString();
        //gnText_TargetNode.text = System.Math.Round(totalGn_targetNode, 2).ToString();
    }

    public void SetCurrentNode()
    {
        if(!useDijkstra)
        {
            for (int i = 0; i < RouteNodeList_AStar.Count; i++)
            {
                if (!RouteNodeList_AStar[i].visited)
                {
                    currentNode = RouteNodeList_AStar[i];
                    RouteNodeList_AStar[i].SetVisited();
                    break;
                }
            }
        }
        else
        {
            for (int i = 0; i < RouteNodeList_Dijkstra.Count; i++)
            {
                if (!RouteNodeList_Dijkstra[i].visited)
                {
                    currentNode = RouteNodeList_Dijkstra[i];
                    RouteNodeList_Dijkstra[i].SetVisited();
                    break;
                }
            }
            
        }
        

        if (currentNode != null)
        {
            currentNode.SetNodeStatus(GridNode.status.CURRENTNODE);
        }

        GetNodeGridIndex(currentNode, gridObjects);
        currentNodeText.text = "( " + currentNode.node.X + " , " + targetNode.node.Y + ")";


    }

    public void GetCurrentNodeIndex(GridNode currentNode)
    {
        GetNodeGridIndex(currentNode, gridObjects);
        currentNodeText.text = "( " + currentNode.node.X + " , " + currentNode.node.Y + ")";
    }

    public void CreateAStarRoute()
    {
        //Initialize graph objects
        RouteNodeList_Dijkstra = graphNodes;
        RouteNodeList_AStar = graphNodes;
        pathNodes = new AStar_Node[graphNodes.Count];


        //if (fnText_TargetNode.text == "(x,y)")
        //{
        //    fnText_TargetNode.text = System.Math.Round(path.fn, 2).ToString();
        //    //  UnityEngine.Debug.Log("Setting total path default target fn text");
        //}

        //Sort the RouteNodeList list of grid nodes so that it matches the A* route array
        RouteNodeList_AStar = SortRouteByAStar(graphNodes, targetNode);

        //RouteNodeList_Dijkstra = SortRouteByShortestPathToTarget(graphNodes, targetNode);


        //Set the current node to the first index in the grid (unless that node is the target node)
        //if (useDijkstra)
        //{
        //    currentNode = RouteNodeList_Dijkstra[0];
        //}
        //else
        //{
            currentNode = RouteNodeList_AStar[0];
        //}
    }

    /// <summary>
    /// Cycles through a list of nodes to find shortest path using A* algorithm
    /// </summary>
    /// <param name="graphNodes"></param>
    /// <param name="destinationNode"></param>
    /// <returns></returns>
    private List<GridNode> SortRouteByAStar(List<GridNode> gridList, GridNode destinationNode)
    {
        //Init values for f(n), g(n), and each node's h(n)
        double fn = 0;
        double gn = 0;
        double hn = 0;

        //The finally sorted route list
        List<GridNode> sortedRoute = new List<GridNode>();

        Stopwatch sw = new Stopwatch();
        //Start of simulation runtime analysis
        sw.Start();


        for (int i = 0; i < gridList.Count; i++)
        {
            pathNodes[i] = gridList[i].GetComponent<AStar_Node>();
            pathNodes[i].edges = gridList[i].GetEdges();

            //Add all the edges for each node to total g(n)
            for (int j = 0; j < pathNodes[i].edges.Length; j++)
            {
                gn += pathNodes[i].edges[j].gn;
            }
        }

        //Assign X,Y coordinates for each grid node index
        foreach (GridNode n in gridList)
        {
            GetNodeGridIndex(n, gridObjects);
        }

        AStar_Node[,] graphNodeArr = new AStar_Node[gridSize, gridSize];
        //Create an A* graph and route from the grid's graph in world space
        for (int y = 0; y < gridSize; y++)
        {
            for (int x = 0; x < gridSize; x++)
            {
                int index = x + y * gridSize;
                GridNode n = gridObjects[index].GetComponent<GridNode>();
                if (n != null)
                {
                    graphNodeArr[x, y] = n.node;
                  
                }
            }
        }

        graph = new AStar_Graph(graphNodeArr);
    
        //Get the total h(n) by iterating through the graph
        //Create an A* graph and route from the grid's graph in world space
        for (int y = 0; y < gridSize; y++)
        {
            for (int x = 0; x < gridSize; x++)
            {
                int index = x + y * gridSize;
                if(gridList.Count > index && gridList.Count > 0)
                {
                    if (gridList[index] != null)
                    {
                        gridList[index].node.hn = Math.Round((Math.Sqrt(Math.Pow((gridList[index].node.X - x), 2) + Math.Pow((gridList[index].node.Y - y), 2))), 2);
                        //  gridList[index].hn = graph.nodes[x, y].hn;
                        sortedRoute.Add(gridList[index]);
                    }
                }
               
             
            }
        }

        AStar_Route route = new AStar_Route(pathNodes, gn);
        fn = route.fn;

        //End of simulation runtime analysis
        sw.Stop();
        System.TimeSpan ts = sw.Elapsed;

        if (destinationNode == targetNode)
        {
            if (aStarTimerTxt != null)
            {
                aStarTimerTxt.text = ts.ToString("mm\\:ss\\.ff");
            }

            //Calculate total f(n), g(n), h(n)
            for (int i = 0; i < sortedRoute.Count; i++)
            {
                fn += gn + sortedRoute[i].hn;
            }

            fnText_TargetNode.text = Math.Round(fn, 2).ToString();
            gnText_TargetNode.text = Math.Round(gn, 2).ToString();
        }
        else if (destinationNode == currentNode)
        {
            //Calculate total f(n), g(n), h(n)
            for (int i = 0; i < sortedRoute.Count; i++)
            {
                fn += gn + sortedRoute[i].hn;
            }

            currentNodeText.text = Math.Round(fn, 2).ToString();
            currentNodeText.text = Math.Round(gn, 2).ToString();
        }
        return sortedRoute;
    }

    /// <summary>
    /// Cycles through all the nodes in the gridList and sorts them according to their
    /// edges' weights by using Dijkstra's algorithm
    /// </summary>
    /// <param name="gridList"></param>
    private List<GridNode> SortRouteByShortestPathToTarget(List<GridNode> gridList, GridNode destinationNode)
    {
        //Init values for f(n), g(n), and each node's h(n)
        double fn = 0;
        double gn = 0;
        double hn = 0;

        //Set the hn to estimate the total cost to target node first
        foreach (GridNode n in gridList)
        {
            hn += n.SetNodeHN(gridList, destinationNode);
        }

        //Use a stopwatch to track elapsed time for Dijkstra's algorithm
        Stopwatch sw = new Stopwatch();
        sw.Start();
        //The finally sorted route list
        List<GridNode> sortedRoute = new List<GridNode>();
        //Create a Queue to track all nodes in the list
        Queue<GridNode> q = new Queue<GridNode>();
        //List of visited nodes
        List<GridNode> visited = new List<GridNode>();

        //Populate the queue with all the nodes between gridList[0]
        //to the end until it reaches target node (exclusive)
        foreach (GridNode g in gridList)
        {
            if (g != targetNode)
            {
                q.Enqueue(g);
            }
        }
        //Make sure the targetNode is the FINAL node added to the queue!
        q.Enqueue(targetNode);

        while (q != null)
        {
            //Pop the current node from the queue
            //that's not already in the set of visited nodes
            //from q with the smallest distance
            if (visited != null && !visited.Contains(q.Peek()))
            {
                GridNode current = q.Dequeue().GetClosestNeighbor();
                visited.Add(current);

                foreach (GridNode n in current.neighbors)
                {
                    double[] currentDistances = current.GetNeighborDistances();
                    double currentMinDist = currentDistances[0];

                    for (int i = 0; i < currentDistances.Length; i++)
                    {
                        double currentNeighborDist = n.GetNeighborEdgeDistance(current);
                        if (currentNeighborDist < currentMinDist)
                        {
                            currentMinDist = currentNeighborDist;
                            sortedRoute.Add(n);
                            gn += currentMinDist;

                        }
                    }
                }
            }
        }

        sw.Stop();
        System.TimeSpan ts = sw.Elapsed;

        if (destinationNode == targetNode)
        {
            if (dijkstraTimerTxt != null)
            {
                dijkstraTimerTxt.text = ts.ToString("mm\\:ss\\.ff");
            }

            //Calculate total f(n), g(n), h(n)
            for (int i = 0; i < sortedRoute.Count; i++)
            {
                fn += gn + sortedRoute[i].hn;
            }

            fnText_TargetNode.text = Math.Round(fn, 2).ToString();
            gnText_TargetNode.text = Math.Round(gn, 2).ToString();
        }
        else if (destinationNode == currentNode)
        {
            //Calculate total f(n), g(n), h(n)
            for (int i = 0; i < sortedRoute.Count; i++)
            {
                fn += gn + sortedRoute[i].hn;
            }

            currentNodeText.text = Math.Round(fn, 2).ToString();
            currentNodeText.text = Math.Round(gn, 2).ToString();
        }


        return sortedRoute;
    }



    /// <summary>
    /// Spawns nodes on a random number of grid spaces based on gridSize
    /// </summary>
    public void SpawnNodes()
    {
        int spawnCount = 0;

        while (spawnCount <= gridSize)
        {
            int randomSpace = UnityEngine.Random.Range(0, spaces.Length);

            //Instantiate the NodePrefab object as a child of the space at
            //a given index IF that index isn't already occupied by another space
            if (spaces[randomSpace].transform.childCount <= 0 && transform.GetChild((randomSpace)).transform.childCount == 0)
            {
                Instantiate(NodePrefab, transform.GetChild((randomSpace)).transform);
                spawnCount++;

            }

        }
        InitializeNodes();
        SetTargetNode();
    }

    private void InitializeNodes()
    {
        for (int y = 0; y < gridSize; y++)
        {
            for (int x = 0; x < gridSize; x++)
            {
                int index = x + y * gridSize;

                //Initialize the AStar_Node component on each grid space object
                if (transform.GetChild(index).transform.childCount > 0)
                {
                    GridNode indexNode = transform.GetChild((index)).transform.GetChild(0).GetComponent<GridNode>();
                    indexNode.InitializeGridNode(x, y);
                    //Add each graph node to the list as its created
                    graphNodes.Add(indexNode);
                }

            }
        }
    }
}
